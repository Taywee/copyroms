from collections.abc import Callable, Iterable, MutableSequence, Sequence
import asyncio
from concurrent.futures import Executor, ThreadPoolExecutor
from functools import partial
from io import TextIOBase
from shutil import copyfile
import locale
import argparse
import re
from typing import Pattern, TextIO
from contextlib import contextmanager, nullcontext
from shlex import quote
from fcntl import flock, LOCK_EX, LOCK_UN
from pathlib import Path
from subprocess import PIPE, DEVNULL, CalledProcessError
import sys
import logging
from typing import Any, TypeVar, cast
from os import cpu_count
from contextlib import suppress

logger = logging.getLogger(__name__)

class NotBrokenStream:
    '''A stream wrapper for stream output, which will silently suppress BrokenPipeError.
    '''
    __slots__ = ('__stream')

    def __init__(self, stream: TextIO) -> None:
        self.__stream = stream

    def write(self, *args, **kwargs) -> int | None:
        try:
            if not self.__stream.closed:
                return self.__stream.write(*args, **kwargs)
        except BrokenPipeError:
            with suppress(BrokenPipeError):
                self.__stream.close()


    def flush(self, *args, **kwargs) -> None:
        try:
            if not self.__stream.closed:
                self.__stream.flush(*args, **kwargs)
        except BrokenPipeError:
            with suppress(BrokenPipeError):
                self.__stream.close()

def setup_logging(verbosity: int):
    rootlogger = logging.getLogger()
    level = logging.WARNING - (verbosity * 10)
    rootlogger.setLevel(level)
    formatter = logging.Formatter('{levelname}: {message}', style='{')
    out=logging.StreamHandler(NotBrokenStream(sys.stdout))
    out.setLevel(logging.DEBUG)
    out.setFormatter(formatter)
    out.addFilter(lambda record: record.levelno < logging.WARNING)
    err=logging.StreamHandler(NotBrokenStream(sys.stderr))
    err.setLevel(logging.WARNING)
    err.setFormatter(formatter)

    rootlogger.handlers = [
        out,
        err,
    ]

def convert_path(source: Path, legal: Pattern, replace: str, output: Path):
    if not source.exists():
        raise RuntimeError('file must exist')

    filtered_name = ''
    for char in source.name:
        if legal.match(char):
            filtered_name += char
        else:
            filtered_name += replace.format(ord(char))

    first_char = filtered_name.lower()[0]
    if ord('a') <= ord(first_char) <= ord('z'):
        directory = first_char
    else:
        directory = '0'

    return output / directory / filtered_name

def copy(
    source: Path,
    output: Path,
    legal: Pattern,
    replace: str,
    dry_run: bool,
):
    source = source.resolve()

    destination = convert_path(
        source=source,
        legal=legal,
        replace=replace,
        output=output,
    )

    if destination.exists():
        logging.debug('Skipping existing %s', quote(str(destination)))
        return

    logging.info('Copying %s to %s', source, destination)
    if not dry_run:
        destination.parent.mkdir(parents=True, exist_ok=True)

        copyfile(source, destination)

async def amain():
    parser = argparse.ArgumentParser(description='Use ffmpeg to convert music into a destination, skipping existing files')
    parser.add_argument('-d', '--dry-run', action='store_true', help="Don't run ffmpeg")
    parser.add_argument('-q', '--quiet', action='count', help="decrease verbosity, specified up to 2 times.", default=0)
    parser.add_argument('-v', '--verbose', action='count', help="increase verbosity, specified up to 2 times.", default=0)
    parser.add_argument('-o', '--output', help='The destination directory. default: %(default)s.', type=Path, default=Path('.'))
    parser.add_argument('-l', '--legal', help='regex matching legal characters in output path and filenames, non-matching codepoints will be encoded. default: %(default)r', default='.')
    parser.add_argument('-r', '--replace', help='Replacement format string for replacing illegal characters, passed the codepoint, default: %(default)r', default='_{:X}_')
    parser.add_argument('input', help='The input files', type=Path, nargs='+')
    args = parser.parse_args()

    input: Sequence[Path] = sorted(args.input)
    output: Path = args.output
    dry_run: bool = args.dry_run
    verbosity: int = args.verbose - args.quiet
    legal = re.compile(args.legal)
    replace: str = args.replace

    setup_logging(verbosity)

    tasks: MutableSequence[asyncio.Future]  = []

    loop = asyncio.get_running_loop()

    with ThreadPoolExecutor(cpu_count() or 1) as executor:
        for source in input:
            tasks.append(loop.run_in_executor(executor, partial(copy,
                source=source,
                output=output,
                legal=legal,
                replace=replace,
                dry_run=dry_run,
            )))

        for task in tasks:
            await task

def main():
    try:
        asyncio.run(amain())
    except KeyboardInterrupt:
        logger.error('Aborted')
        sys.exit(1)

if __name__ == '__main__':
    main()
